#!/usr/bin/env bash

set -o errexit
set -o nounset
set -o pipefail

if [[ "${TRACE-0}" == "1" ]]; then set -o xtrace; fi

if [[ "${1-}" =~ ^-*h(elp)?$ ]]; then
  echo 'Usage: ./deploy-to-kubernetes.sh dev eregcms els test BETA seekrit

This script attempts to parse kubernetes entities from deploy, service, secrets... files, replace placeholders
in them with public environment variables but also secrets decrypted from an openssl base64. It will then try
to apply the correct image to the k8s deployment and restart haproxy.

'
  exit
fi

cd "$(dirname "$0")"

main() {
  # dev, live?
  cluster=$1
  # eregcms
  repo=$2
  # els
  country=$3
  # dev, beta, test, prelive, preview, production/live
  env=$4
  # DEV, RC, BETA, TEST, MASTER, 1.111.11
  version=$5

  secret_key=$6

  echo 'NAMESPACE='${cluster}'\n' >>k8s/.env
  cat k8s/common.properties >>k8s/.env
  echo '\n' >>k8s/.env
  cat k8s/${country}.${env}.properties >>k8s/.env
  echo '\n' >>k8s/.env

  deploy_image=unctad/${repo}:${version}
  echo IMAGE=${deploy_image}'\n' >>k8s/.env

  openssl enc -d -base64 -aes-256-cbc -md sha256 -pass pass:${secret_key} -in k8s/${country}.${env}.secrets >>k8s/.env

  # kubectl='kubectl --server='${cluster}' --token='${jenkins_token}''

  for file in deployment service secrets; do
    echo "${file}"
    awk -f k8s/replace-placeholders.awk ${repo}/k8s/${country}.${env}.properties ${repo}/k8s/${file}.yml >${file}_replaced.yml
    ${kubectl} apply -f ${file}_replaced.yml --force
    rm -rf ${file}_replaced.yml
  done

  # Clean up our secrets
  rm -rf ${repo}/k8s/${country}.${env}.properties

  ${kubectl} set image deployment/ereg-cms-frontend ereg-cms-frontend=${deploy_image}

  if ${kubectl} rollout status deployment ereg-cms-frontend | grep -q 'successfully rolled out'; then
    echo "deployment succeeded"
  else
    echo "deployment failed"
    exit 1
  fi

  rm -rf k8s
}

main "$@"
