# Execute this from the location of the .env file
import os
import sys
from datetime import date

print('Running .env backup')

if not os.path.exists(".env"):
    sys.exit('No .env file in %s, aborting .env backup' % os.getcwd())

today = date.today()
f_name = init_f_name = '.env-backup-%s' % today.strftime("%Y%m%d")
counter = 1
while True:
    if os.path.exists(f_name):
        f_name = init_f_name + "-" + str(counter)
        counter += 1
    else:
        os.system('cp .env %s' % f_name)
        print('Successfully backed up .env to %s' % f_name)
        break
