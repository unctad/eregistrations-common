# Execute this from the location of the .env file
import time
import requests
import subprocess
from datetime import datetime

# This script is meant to be run from cron in intervals of few minutes:
# example setup in crontab:
# */5 * * * * python3 /home/jenkins/eregistrations/eregistrations-common/scripts/restart_services.py >> /home/jenkins/eregistrations/service_restart.log 2>&1

def restart_mules():
    try:
        result = subprocess.run("docker ps | grep mule", shell=True, stdout=subprocess.PIPE, encoding='utf8')
        result.check_returncode()
        out = result.stdout

        out_list = out.splitlines()
        for line in out_list:
            parsed_line = line.split()
            if len(parsed_line) < 2:
                continue
            if not "mule" in parsed_line[1]:
                continue

            print("Will now restart mule id: %s, at: %s" % (parsed_line[0], str(datetime.now())))
            result = subprocess.run("docker restart %s" % parsed_line[0], shell=True, stdout=subprocess.PIPE, encoding='utf8')
            result.check_returncode()
            # below is to allow some time for previously restarted instance to get up,
            # so that resources are not consumed by all instances at the same time
            time.sleep(30)

    except Exception as ex:
        print("Problem restarting mules: %s" % ex)


def main():
    try:
        response = requests.get('http://172.18.0.1:8081/api/health')
        response.raise_for_status()
        if  response.json().get("status") != 'UP':
            raise Exception("Bad status from mule")
    except Exception as ex:
        print("Unreachable health endpoint or bad status, will restart mules: %s" %ex)
        restart_mules()



main()