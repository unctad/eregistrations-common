# Execute this from the location of the .env file
import os
import getopt, sys
from datetime import date
import json
import re

'''
This is responsible for swaping versions in the .env file
Usage example:

python3 scripts/swap_versions.py --target_env Conf-LIVE --instance_namespace lesotho --service_branch live_branch

Above would replace .env from Conf-LIVE/compose/lesotho/ with the versions written to live_branch in meta.json
'''

print('Version swap')

try:
    opts, args = getopt.getopt(sys.argv[1:],"s:",["target_env=","instance_namespace=","service_branch=", "services_to_deploy="])
except getopt.GetoptError:
    print('Allowed params: --target_env <target_env> --instance_namespace <instance_namespace> --service_branch <service_branch> --services_to_deploy <services_to_deploy>')
    sys.exit(2)

target_env = None
instance_namespace = None
service_branch = None
services_to_deploy = []

for opt, arg in opts:
    if opt == '--target_env':
        target_env = arg
    if opt == '--instance_namespace':
        instance_namespace = arg
    if opt == '--service_branch':
        service_branch = arg
    if opt == '--services_to_deploy':
        services_to_deploy = arg.split(',')

if target_env is None:
    print('No required param --target_env <target_env>, example: Conf-LIVE')
    sys.exit(2)

if instance_namespace is None:
    print('No required param --instance_namespace <instance_namespace>, example: lesotho')
    sys.exit(2)

if service_branch is None:
    print('No required param --service_branch <service_branch>, example: live_branch (corresponds to top level keys in meta.json)')
    sys.exit(2)

if not services_to_deploy:
    print('No required param --services_to_deploy <services_to_deploy>, example: eregcms,js-assistant')
    sys.exit(2)

print("Will deploy following services: %r" % services_to_deploy)

init_cwd = os.path.abspath(os.getcwd())

env_content = None
meta_content = None
with open(os.path.join(init_cwd, "%s/compose/%s/.env" % (target_env, instance_namespace)), "r+") as env:
    env_content = env.read()
with open('eregistrations-common/meta.json', 'r+') as meta_file:
    meta_content = json.loads(meta_file.read())

for service_key in services_to_deploy:
    service = meta_content[service_branch]['microServicesVersions'][service_key]
    target = service['alias'] + "_VER"
    new_version = service['version']
    env_content = re.sub(rf'\n({target})\s*=\s*.+\n', rf'\n\1={new_version}\n', env_content)

with open(os.path.join(init_cwd, "%s/compose/%s/.env" % (target_env, instance_namespace)), "w") as env:
    env.write(env_content)

print("Swap done")