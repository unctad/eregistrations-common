# Execute this from the location of the .env file
import os
import getopt, sys
import re

'''
This is responsible for swaping eregistrations versions in the .env file
Usage example:

python3 scripts/crud_env_variable.py --target_env Conf-LIVE --instance_namespace lesotho --var_name=RESTHEART_VER --var_value=RC

to delete

python3 scripts/crud_env_variable.py --target_env Conf-LIVE --instance_namespace lesotho --var_name=RESTHEART_VER --var_value=RC --is_delete=True

Above would replace or remove from .env of Conf-LIVE/compose/lesotho/ the RESTHEART_VER variable.
'''

print('Version swap')

try:
    opts, args = getopt.getopt(sys.argv[1:],"s:",["target_env=","instance_namespace=", "var_name=", "var_value=", "is_delete="])
except getopt.GetoptError:
    print('Allowed params: --target_env <target_env> --instance_namespace <instance_namespace> --var_name <var_name> --var_value <var_value> --is_delete <is_delete>')
    sys.exit(2)

target_env = None
instance_namespace = None
var_value = None
var_name = None
is_delete = False

for opt, arg in opts:
    if opt == '--target_env':
        target_env = arg
    if opt == '--instance_namespace':
        instance_namespace = arg
    if opt == '--var_name':
        var_name = arg
    if opt == '--var_value':
        var_value = arg
    if opt == '--is_delete':
        is_delete = bool(arg)

if target_env is None:
    print('No required param --target_env <target_env>, example: Conf-LIVE')
    sys.exit(2)

if instance_namespace is None:
    print('No required param --instance_namespace <instance_namespace>, example: lesotho')
    sys.exit(2)

if var_name is None:
    print('No required param --var_name <var_name>')
    sys.exit(2)

if is_delete is None:
    is_delete = False

if var_value is None and not is_delete:
    print('No var_value param --var_name <var_name>, default is empty string')
    var_value = ''


if is_delete:
    print(f"Will delete following variable from .env: {var_name}")
else:
    print(f"Will setup following variable name/value: {var_name} / {var_value}")



init_cwd = os.path.abspath(os.getcwd())

env_content = None

with open(os.path.join(init_cwd, "%s/compose/%s/.env" % (target_env, instance_namespace)), "r+") as env:
    env_content = env.read()


if is_delete:
    env_content = re.sub(rf'\n({var_name})\s*=\s*.+\n', rf'', env_content)
elif re.findall(rf'\n({var_name})\s*=\s*.+', env_content):
    env_content = re.sub(rf'\n({var_name})\s*=\s*.+\n', rf'\n\1={var_value}\n', env_content)
else:
    env_content += f'\n{var_name}={var_value}\n'

with open(os.path.join(init_cwd, "%s/compose/%s/.env" % (target_env, instance_namespace)), "w") as env:
    env.write(env_content)

if is_delete:
    print(f"ENV variable deleted: {var_name}")
else:
    print(f"ENV variable added/updated: {var_name}={var_value}")
